﻿
namespace Lab_1
{
    public enum atomType
    {
        ID,
        CONST
    }
    public class Simbol
    {
        public string simbol { get; set; }
        public int TScode { get; set; }

        public atomType type { get; set; }
        
        public static int lastCode =0;
        public Simbol(string simbol, atomType type)
        { 
            this.simbol = simbol;
            this.type = type;
            TScode = lastCode++;
        }

        public override string ToString()
        {
            return "Simbol: " + simbol + ", cod TS: " + TScode + ", tip: " + type.ToString();
        }
    }
}