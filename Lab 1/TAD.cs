﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab_1
{
    public class TAD
    {
        private int maxSize;
        private int actualSize;
        private Simbol[] array;

        public TAD(int maxSize)
        {
            this.maxSize = maxSize;
            actualSize = 0;
            array = new Simbol[maxSize];
        }
        private void resize() {
            Simbol[] newArray = new Simbol[2 * maxSize];
            for (int i = 0; i < maxSize; i++) {
                newArray[i] = array[i];
            }
            maxSize = 2 * maxSize;
            array = newArray;
        }
        
        public int add(string simbol, atomType type) {
            if (actualSize == maxSize) resize();
            int i = 0;
            while (i < actualSize && string.Compare(array[i].simbol, simbol) < 0) i++;
            if (i <= actualSize-1 && array[i].simbol == simbol)
                return array[i].TScode;
            for (int j= actualSize; j > i; j--) {
                array[j] = array[j-1];
            }
            actualSize++;
            array[i] = new Simbol(simbol,type);
            return array[i].TScode;
        }

        public List<Simbol> getArray()
        {
            List<Simbol> newArray = new List<Simbol>();
            for(int i=0;i< actualSize;i++)
                newArray.Add(array[i]);
            return newArray;
        }
    }
}