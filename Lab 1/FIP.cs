﻿using System.Text.RegularExpressions;

namespace Lab_1
{
    public class FIP
    {
        public int codAtom { get; set; }
        public int codTS { get; set; }
        
        public string value { get; set; }

        public FIP(int codTS, int codAtom, string value)
        {
            this.codTS = codTS;
            this.codAtom = codAtom;
            this.value = value;
        }

        public static bool isID(string word) {
            return Regex.Match(word,"[a-zA-Z]+").Success && word.Length <= 8;
        }

        public static bool isCONST(string word)
        {
            return Regex.Match(word, "[-+]?[0-9]+([.][0-9]+)?").Success;
        }

        public override string ToString()
        {
            return "Cod atom: " + codAtom + ", cod TS: " + codTS + ", value: " + value;
        }
    }
}