﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Lab_1
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            //language=regexp
            //const string beginProgram = @"#include[ ]+<iostream>\s*\n\s*using[ ]namespace[ ]std;\s*\n\s*int\s+main\(\)\s+\{";
            //language=regexp
            const string imports = "(#include|<iostream>|using|namespace|std)";
            //language=regexp
            const string endProgram = @"return";
            //language=regexp
            const string numericConst = "[-+]?[0-9]+([.][0-9]+)?";
            //language=regexp
            const string varName = "[a-zA-Z]+";
            //language=regexp
            const string separators = "[(){},;]";
            //language=regexp
            const string operators = @"([+\-*/=]|>>|<<)";
            //language=regexp
            const string instructionTypes = "(int|double|struct|if|while|cin|cout)";
            //language=regexp
            const string inOutOperators = "(<<|>>)";
            //language=regexp
            const string conditionalOperators = @"(\|\||&&|==|<=|>=|>|<|!=)";
            //language=regexp
            string possibleExpressions =
                $"^{imports}|({instructionTypes}|{numericConst}|{varName}|" +
                $"{inOutOperators}|{separators}|{conditionalOperators}|{operators})|{endProgram}";

            string fileInput;
            using (var reader = new StreamReader("../../input.txt"))
                fileInput = reader.ReadToEnd();
            
            TAD ts = new TAD(10);
            List<FIP> fipTable = new List<FIP>();
            Dictionary<string, int> atomsTable = createTable();

            string[] lines = Regex.Split(fileInput, "\n+");
            foreach (var line in lines)
            {
                string[] possibleAtoms = Regex.Split(line, "\\s+");
                foreach (var possibleAtom in possibleAtoms)
                {
                    var possibleAtomCopy = possibleAtom;
                    while (!string.IsNullOrWhiteSpace(possibleAtomCopy))
                    {
                        var regexExpr = new Regex(possibleExpressions);
                        var atom = regexExpr.Match(possibleAtomCopy).Value;

                        if (string.IsNullOrEmpty(atom))
                            Console.WriteLine("Eroare lexicala la atomul " + possibleAtom + " din linia: " + line);
                        
                        if (string.IsNullOrWhiteSpace(atom)) 
                            break;

                        possibleAtomCopy = regexExpr.Replace(possibleAtomCopy, "", 1);
                    
                        int idValue =0;
                        bool valid = false;
                        if (atomsTable.TryGetValue(atom,out idValue)) {
                            fipTable.Add(new FIP(-1,idValue,atom));
                            valid = true;
                        }
                        else if (FIP.isID(atom)) {     
                            var id = ts.add(atom,atomType.ID);
                            fipTable.Add(new FIP(id,idValue,atom));
                            valid = true;
                        }
                        else if (FIP.isCONST(atom)) { 
                            var id = ts.add(atom,atomType.CONST);
                            fipTable.Add(new FIP(id,idValue,atom));
                            valid = true;
                        }

                        if (!valid)
                            Console.WriteLine("Eroare lexicala la atomul " + atom + " din linia: " + line);
                    }
                    
                }

            }

            Console.WriteLine();
            
            foreach (var atom in fipTable)
                Console.WriteLine(atom);

            Console.WriteLine();
            foreach (var item in ts.getArray())
                Console.WriteLine(item);

        }
        
        public static Dictionary<string, int> createTable()
        {
            Dictionary<string, int> table = new Dictionary<string, int>();
            table.Add("ID", 0);
            table.Add("CONST", 1);
            table.Add("int", 2);
            table.Add("double", 3);
            table.Add("struct", 4);
            table.Add("(", 5);
            table.Add(")", 6);
            table.Add("*", 7);
            table.Add("/", 8);
            table.Add("+", 9);
            table.Add("-", 10);
            table.Add("!=", 11);
            table.Add("=", 12);
            table.Add("==", 13);
            table.Add(">", 14);
            table.Add("<", 15);
            table.Add("<=", 15);
            table.Add(">=", 16);
            table.Add("{", 17);
            table.Add("}", 18);
            table.Add(";", 19);
            table.Add(",", 20);
            table.Add("if", 21);
            table.Add("else", 22);
            table.Add("while", 23);
            table.Add("cin", 24);
            table.Add("cout", 25);
            table.Add(">>", 26);
            table.Add("<<", 27);
            table.Add("\n", 28);
            table.Add("return", 29);
            table.Add("#include", 30);
            table.Add("using", 31);
            table.Add("namespace", 32);
            table.Add("std", 33);
            table.Add("<iostream>",34);
            table.Add("main", 35);
            return table;
        }
    }
}